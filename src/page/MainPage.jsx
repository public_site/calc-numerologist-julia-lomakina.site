import { Box, Button, Container, Grid, Typography } from "@mui/material";
import { display } from "@mui/system";
import React from "react";
import MatrixCompatible from "../component/Matrix/MatrixCompatible";
import MatrixIndividual from "../component/Matrix/MatrixIndividual";
import ImgAvatar from "../img/avatar.jpg";
import styles from "./Page.module.scss";

export default function MainPage() {
  const [Matrix, setMatrix] = React.useState("");

  return (
    <Container>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Box sx={{ margin: 5, display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
            <img src={ImgAvatar} className={styles.Avatar} />
          </Box>
          <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
            <Box className={styles.Title}>
              <Typography color="primary" variant="h3">
                Калькулятор расчета
              </Typography>
              <Typography color="primary" variant="h3">
                Матрицы Судьбы
              </Typography>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} container spacing={3}>
          <Grid item xs={12} md={6} sx={{ display: "flex", justifyContent: "end", "@media (max-width: 800px)": { justifyContent: "center" } }}>
            <Button variant="contained" onClick={(e) => setMatrix("MatrixIndividual")}>
              Индивидуальный расчет
            </Button>
          </Grid>
          <Grid item xs={12} md={6} sx={{ display: "flex", justifyContent: "start", "@media (max-width: 800px)": { justifyContent: "center" } }}>
            <Button variant="contained" onClick={(e) => setMatrix("MatrixCompatible")}>
              Расчет совместимости
            </Button>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Box>
            {Matrix === "MatrixIndividual" ? <MatrixIndividual /> : null}
            {Matrix === "MatrixCompatible" ? <MatrixCompatible /> : null}
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}
