import React from "react";

export default function CanvasMatrix() {
  const canvasRef = React.useRef();

  React.useEffect(() => {
    const ctx = canvasRef.current.getContext("2d");
    ctx.fillRect(0, 0, 100, 100);
  }, []);

  return <canvas ref={canvasRef} width={300} height={300} />;
}
