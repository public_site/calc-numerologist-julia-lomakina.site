import React from "react";
import styles from "./Matrix.module.scss";

import { Box, Grid, Typography } from "@mui/material";
import Diagram from "../Diagram/Diagram";
import { IMaskInput } from "react-imask";
import Input from "@mui/material/Input";
import FormControl from "@mui/material/FormControl";
import { CalcMatrixForString } from "../../funcs";

const TextMaskCustom = React.forwardRef(function TextMaskCustom(props, ref) {
  const { onChange, ...other } = props;
  return (
    <IMaskInput
      {...other}
      mask="##.##.####"
      definitions={{
        "#": /[0-9]/,
      }}
      inputRef={ref}
      onAccept={(value) => onChange({ target: { name: props.name, value } })}
      overwrite
    />
  );
});

export default function MatrixIndividual() {
  const [Value, setValue] = React.useState();
  const [Data, setData] = React.useState([]);

  const handleChange = (event) => {
    var value = event.target.value;
    setValue(event.target.value);
    if (value.length === 10) {
      setData(CalcMatrixForString(value));
    }
  };

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <Box sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
            <FormControl variant="standard">
              <Input value={Value} onChange={handleChange} className={styles.InputDate} inputComponent={TextMaskCustom} />
            </FormControl>
            <Typography>Введите дату рождения</Typography>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
            <Diagram data={Data} />
          </Box>
        </Grid>
      </Grid>
    </>
  );
}
