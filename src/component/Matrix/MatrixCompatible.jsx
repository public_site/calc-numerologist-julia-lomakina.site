import React from "react";
import styles from "./Matrix.module.scss";
import Diagram from "../Diagram/Diagram";
import DiagramCompatible from "../Diagram/DiagramCompatible";
import TextField from "@mui/material/TextField";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";

import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import ruLocale from "date-fns/locale/ru";
import { Box, Grid, Typography } from "@mui/material";

import { IMaskInput } from "react-imask";
import Input from "@mui/material/Input";
import FormControl from "@mui/material/FormControl";
import { CalcMatrixForString, calcMatrixCompatibility } from "../../funcs";

const TextMaskCustom = React.forwardRef(function TextMaskCustom(props, ref) {
  const { onChange, ...other } = props;
  return (
    <IMaskInput
      {...other}
      mask="##.##.####"
      definitions={{
        "#": /[0-9]/,
      }}
      inputRef={ref}
      onAccept={(value) => onChange({ target: { name: props.name, value } })}
      overwrite
    />
  );
});

export default function MatrixCompatible() {
  const [Value1, setValue1] = React.useState();
  const [Value2, setValue2] = React.useState();
  const [Data1, setData1] = React.useState([]);
  const [Data2, setData2] = React.useState([]);
  const [Data3, setData3] = React.useState([]);

  const handleChange1 = (event) => {
    var value = event.target.value;
    setValue1(event.target.value);
    if (value.length === 10) {
      setData1(CalcMatrixForString(value));
    }
  };

  const handleChange2 = (event) => {
    var value = event.target.value;
    setValue2(event.target.value);
    if (value.length === 10) {
      setData2(CalcMatrixForString(value));
    }
  };

  React.useEffect(() => {
    if (Data1.length !== 0 && Data2.length !== 0) {
      setData3(calcMatrixCompatibility(Data1, Data2));
    }
  }, [Data1, Data2]);

  var clsContainerMatrix = [styles.ContainerMatrix];
  if (Data1.length === 0 || Data2.length === 0) {
    clsContainerMatrix.push(styles.Hidden);
  }

  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={12} md={6} sx={{ display: "flex", justifyContent: "end", "@media (max-width: 800px)": { justifyContent: "center" } }}>
          <Box sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", mx: 2 }}>
            <FormControl>
              <Input value={Value1} onChange={handleChange1} className={styles.InputDate} inputComponent={TextMaskCustom} />
            </FormControl>
            <Typography>Введите дату рождения 1 партнера</Typography>
          </Box>
        </Grid>
        <Grid item xs={12} md={6} sx={{ display: "flex", justifyContent: "start", "@media (max-width: 800px)": { justifyContent: "center" } }}>
          <Box sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", mx: 2 }}>
            <FormControl>
              <Input value={Value2} onChange={handleChange2} className={styles.InputDate} inputComponent={TextMaskCustom} />
            </FormControl>
            <Typography>Введите дату рождения 2 партнера</Typography>
          </Box>
        </Grid>
      </Grid>

      <Box className={clsContainerMatrix.join(" ")}>
        <Grid container spacing={1}>
          <Grid item xs={12} sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", mt: 2 }}>
            <Typography variant="h4" color="primary">
              Матрица совместимости
            </Typography>
            <DiagramCompatible data={Data3} />
          </Grid>
          <Grid item xs={12} md={6} sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
            <Typography variant="h5" color="primary">
              Матрица 1 партнера
            </Typography>
            <Typography variant="h6" color="primary">
              Возраст: {new Date().getFullYear() - new Date(Value1).getFullYear()} год
            </Typography>
            <Diagram data={Data1} />
          </Grid>
          <Grid item xs={12} md={6} sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
            <Typography variant="h5" color="primary">
              Матрица 2 партнера
            </Typography>
            <Typography variant="h6" color="primary">
              Возраст: {new Date().getFullYear() - new Date(Value2).getFullYear()} год
            </Typography>

            <Diagram data={Data2} />
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
