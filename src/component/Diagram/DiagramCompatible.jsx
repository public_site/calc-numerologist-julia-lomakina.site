import React from "react";
import styles from "./Diagram.module.scss";

export default function Diagram({ data }) {
  let values = new Map();
  data.forEach((element, key) => {
    let value = element >= 10 ? element + "" : "\u00A0" + element;
    values.set(key, value);
  });
  var cls = [styles.Diagram, styles.Hidden];
  if (data.length !== 0) {
    cls = [styles.Diagram];
  }
  return (
    <svg xmlns="http://www.w3.org/2000/svg" id="matrix-svg" className={styles.Diagram} viewBox="-15 -15 530 530" width={500}>
      <g>
        <rect x="100" y="100" width="300" height="300" fill="transparent" stroke="black" />
        <line x1="100" y1="100" x2="400" y2="400" stroke="black" />
        <line x1="400" y1="100" x2="100" y2="400" stroke="black" />
        <line x1="250" y1="400" x2="400" y2="250" stroke="black" />
      </g>

      <g className={styles.React2}>
        <rect x="100" y="100" width="300" height="300" fill="transparent" stroke="black" />
        <line x1="100" y1="100" x2="400" y2="400" stroke="black" />
        <line x1="400" y1="100" x2="100" y2="400" stroke="black" />
      </g>

      <g style={{ transform: "rotate(293deg)", transformBox: "fill-box", transformOrigin: "left center" }}>
        <line x1="0" y1="250" x2="190" y2="250" stroke="#000" />
        <circle cx="35" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="55" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="75" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="95" cy="250" r="3" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="115" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="135" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="155" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
      </g>

      <g style={{ transform: "rotate(-23deg)", transformBox: "fill-box" }}>
        <line x1="75" y1="75" x2="265" y2="75" stroke="#000" />
        <circle cx="110" cy="75" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="130" cy="75" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="150" cy="75" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="170" cy="75" r="3" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="190" cy="75" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="210" cy="75" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="230" cy="75" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
      </g>

      <g style={{ transform: "rotate(23deg)", transformBox: "fill-box" }}>
        <line x1="250" y1="0" x2="440" y2="0" stroke="#000" />
        <circle cx="285" cy="0" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="305" cy="0" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="325" cy="0" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="345" cy="0" r="3" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="365" cy="0" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="385" cy="0" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="405" cy="0" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
      </g>

      <g style={{ transform: "rotate(67deg)", transformBox: "fill-box" }}>
        <line x1="428" y1="83" x2="608" y2="83" stroke="#000" />
        <circle cx="458" cy="83" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="478" cy="83" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="498" cy="83" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="518" cy="83" r="3" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="538" cy="83" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="558" cy="83" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="578" cy="83" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
      </g>

      <g style={{ transform: "rotate(113deg)", transformBox: "fill-box" }}>
        <line x1="503" y1="253" x2="693" y2="253" stroke="#000" />
        <circle cx="535" cy="253" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="555" cy="253" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="575" cy="253" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="595" cy="253" r="3" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="615" cy="253" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="635" cy="253" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="655" cy="253" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
      </g>

      <g style={{ transform: "rotate(157deg)", transformBox: "fill-box" }}>
        <line x1="425" y1="431" x2="615" y2="431" stroke="#000" />
        <circle cx="425" cy="431" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="455" cy="431" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="475" cy="431" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="495" cy="431" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="515" cy="431" r="3" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="535" cy="431" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="555" cy="431" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="575" cy="431" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="595" cy="431" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
      </g>

      <g style={{ transform: "rotate(203deg)", transformBox: "fill-box" }}>
        <line x1="250" y1="505" x2="443" y2="505" stroke="#000" />
        <circle cx="285" cy="505" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="305" cy="505" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="325" cy="505" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="345" cy="505" r="3" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="365" cy="505" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="385" cy="505" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="405" cy="505" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
      </g>

      <line x1="75" y1="425" x2="0" y2="250" stroke="#acacac" />
      <g style={{ transform: "rotate(67deg)", transformBox: "fill-box", transformOrigin: "left center" }}>
        <line x1="0" y1="250" x2="190" y2="250" stroke="#000" />
        <circle cx="35" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="55" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="75" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="95" cy="250" r="3" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="115" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="135" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
        <circle cx="155" cy="250" r="2" fill="#000" stroke="#fff" style={{ opacity: 0 }} />
      </g>

      <g className={styles.React2}>
        <circle cx="100" cy="100" r="24" fill="#939" stroke="#fff" style={{ transform: "translate(-10px, -10px)" }} />
        <circle cx="100" cy="100" r="18" fill="#36c" stroke="#fff" style={{ transform: "translate(20px, 20px)" }} />
        <circle cx="100" cy="100" r="14" fill="#3eb4f0" stroke="#fff" style={{ transform: "translate(44px, 44px)" }} />
        <circle cx="100" cy="100" r="14" fill="#73b55f" stroke="#fff" style={{ transform: "translate(90px, 90px)" }} />

        <circle cx="400" cy="100" r="24" fill="#d14a44" stroke="#fff" style={{ transform: "translate(10px, -10px)" }} />
        <circle cx="400" cy="100" r="18" fill="white" stroke="black" style={{ transform: "translate(-20px, 20px)" }} />
        <circle cx="400" cy="100" r="14" fill="#d88a4b" stroke="#fff" style={{ transform: "translate(-44px, 44px)" }} />

        <circle cx="400" cy="400" r="24" fill="#d14a44" stroke="#fff" style={{ transform: "translate(10px, 10px)" }} />
        <circle cx="400" cy="400" r="18" fill="white" stroke="black" style={{ transform: "translate(-20px, -20px)" }} />
        <circle cx="400" cy="400" r="14" fill="#d88a4b" stroke="#fff" style={{ transform: "translate(-44px, -44px)" }} />

        <circle cx="100" cy="400" r="24" fill="#939" stroke="#fff" style={{ transform: "translate(-10px, 10px)" }} />
        <circle cx="100" cy="400" r="18" fill="#36c" stroke="#fff" style={{ transform: "translate(20px, -20px)" }} />
        <circle cx="100" cy="400" r="14" fill="#3eb4f0" stroke="#fff" style={{ transform: "translate(44px, -44px)" }} />
        <circle cx="100" cy="400" r="14" fill="#73b55f" stroke="#fff" style={{ transform: "translate(90px, -90px)" }} />
      </g>
      <circle cx="100" cy="100" r="24" fill="white" stroke="black" style={{ transform: "translate(-10px, -10px)" }} />
      <circle cx="100" cy="100" r="18" fill="white" stroke="black" style={{ transform: "translate(20px, 20px)" }} />
      <circle cx="100" cy="100" r="14" fill="white" stroke="black" style={{ transform: "translate(44px, 44px)" }} />

      <circle cx="400" cy="100" r="24" fill="white" stroke="black" style={{ transform: "translate(10px, -10px)" }} />
      <circle cx="400" cy="100" r="18" fill="white" stroke="black" style={{ transform: "translate(-20px, 20px)" }} />
      <circle cx="400" cy="100" r="14" fill="white" stroke="black" style={{ transform: "translate(-44px, 44px)" }} />

      <circle cx="400" cy="400" r="24" fill="white" stroke="black" style={{ transform: "translate(10px, 10px)" }} />
      <circle cx="400" cy="400" r="18" fill="white" stroke="black" style={{ transform: "translate(-20px, -20px)" }} />
      <circle cx="400" cy="400" r="14" fill="white" stroke="black" style={{ transform: "translate(-44px, -44px)" }} />

      <circle cx="100" cy="400" r="24" fill="white" stroke="black" style={{ transform: "translate(-10px, 10px)" }} />
      <circle cx="100" cy="400" r="18" fill="white" stroke="black" style={{ transform: "translate(20px, -20px)" }} />
      <circle cx="100" cy="400" r="14" fill="white" stroke="black" style={{ transform: "translate(44px, -44px)" }} />

      <circle cx="250" cy="400" r="14" fill="#fff" stroke="black" style={{ transform: "translate(40px, -40px)" }} />
      <circle cx="250" cy="400" r="14" fill="#fff" stroke="black" style={{ transform: "translate(75px, -75px)" }} />
      <circle cx="250" cy="400" r="14" fill="#fff" stroke="black" style={{ transform: "translate(110px, -110px)" }} />

      <circle cx="100" cy="100" r="24" fill="#f7de74" stroke="#fff" style={{ transform: "translate(150px, 150px)" }} />

      <g id="points-basic-mode">
        <text className={styles.DiagramValue} style={{ transform: "translate(15px, 256px)" }} fill="#ffffff">
          {values.get("point_01")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 30px)" }} fill="#ffffff">
          {values.get("point_02")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(468px, 256px)" }} fill="#ffffff">
          {values.get("point_03")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(242px, 481px)" }} fill="#ffffff">
          {values.get("point_04")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 256px)" }}>
          {values.get("point_05")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(81px, 95px)" }}>
          {values.get("point_18")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(401px, 95px)" }}>
          {values.get("point_19")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(401px, 416px)" }}>
          {values.get("point_20")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(81px, 416px)" }}>
          {values.get("point_21")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(242px, 406px)" }} fill="#ffffff">
          {values.get("point_09")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(391px, 256px)" }} fill="#ffffff">
          {values.get("point_08")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(316px, 331px)" }}>
          {values.get("point_10")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(281px, 366px)" }}>
          {values.get("point_12")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(351px, 296px)" }}>
          {values.get("point_11")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(91px, 256px)" }} fill="#ffffff">
          {values.get("point_07")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 106px)" }} fill="#ffffff">
          {values.get("point_06")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(57px, 256px)" }} fill="#ffffff">
          {values.get("point_15")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 72px)" }} fill="#ffffff">
          {values.get("point_14")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(242px, 439px)" }}>
          {values.get("point_13")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(156px, 256px)" }} fill="#ffffff">
          {values.get("point_17")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 171px)" }} fill="#ffffff">
          {values.get("point_16")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(112px, 126px)" }}>
          {values.get("point_c1_10_2")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(371px, 126px)" }}>
          {values.get("point_c1_30_2")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(371px, 386px)" }}>
          {values.get("point_c1_50_2")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(112px, 386px)" }}>
          {values.get("point_c1_70_2")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(136px, 150px)" }}>
          {values.get("point_c1_10_3")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(347px, 150px)" }}>
          {values.get("point_c1_30_3")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(347px, 362px)" }}>
          {values.get("point_c1_50_3")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(136px, 362px)" }}>
          {values.get("point_c1_70_3")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(424px, 256px)" }}>
          {values.get("point_c1_40_2")}
        </text>
      </g>
    </svg>
  );
}
