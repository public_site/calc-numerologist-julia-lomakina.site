import React from "react";
import styles from "./Diagram.module.scss";

export default function Diagram({ data }) {
  let values = new Map();
  data.forEach((element, key) => {
    let value = element >= 10 ? element + "" : "\u00A0" + element;
    values.set(key, value);
  });

  return (
    <svg xmlns="http://www.w3.org/2000/svg" id="matrix-svg" className={styles.Diagram} viewBox="-15 -15 530 530" width={730}>
      <g>
        <rect x="100" y="100" width="300" height="300" fill="transparent" stroke="black" />
        <line x1="100" y1="100" x2="400" y2="400" stroke="black" />
        <line x1="400" y1="100" x2="100" y2="400" stroke="black" />
        <line x1="250" y1="400" x2="400" y2="250" stroke="black" />
      </g>

      <g className={styles.React2}>
        <rect x="100" y="100" width="300" height="300" fill="transparent" stroke="black" />
        <line x1="100" y1="100" x2="400" y2="400" stroke="black" />
        <line x1="400" y1="100" x2="100" y2="400" stroke="black" />
      </g>

      <g style={{ transform: "rotate(293deg)", transformBox: "fill-box", transformOrigin: "left center" }}>
        <line x1="0" y1="250" x2="190" y2="250" stroke="#000" />
        <circle cx="35" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="55" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="75" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="95" cy="250" r="3" fill="#000" stroke="#fff" />
        <circle cx="115" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="135" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="155" cy="250" r="2" fill="#000" stroke="#fff" />
      </g>

      <g style={{ transform: "rotate(-23deg)", transformBox: "fill-box" }}>
        <line x1="75" y1="75" x2="265" y2="75" stroke="#000" />
        <circle cx="110" cy="75" r="2" fill="#000" stroke="#fff" />
        <circle cx="130" cy="75" r="2" fill="#000" stroke="#fff" />
        <circle cx="150" cy="75" r="2" fill="#000" stroke="#fff" />
        <circle cx="170" cy="75" r="3" fill="#000" stroke="#fff" />
        <circle cx="190" cy="75" r="2" fill="#000" stroke="#fff" />
        <circle cx="210" cy="75" r="2" fill="#000" stroke="#fff" />
        <circle cx="230" cy="75" r="2" fill="#000" stroke="#fff" />
      </g>

      <g style={{ transform: "rotate(23deg)", transformBox: "fill-box" }}>
        <line x1="250" y1="0" x2="440" y2="0" stroke="#000" />
        <circle cx="285" cy="0" r="2" fill="#000" stroke="#fff" />
        <circle cx="305" cy="0" r="2" fill="#000" stroke="#fff" />
        <circle cx="325" cy="0" r="2" fill="#000" stroke="#fff" />
        <circle cx="345" cy="0" r="3" fill="#000" stroke="#fff" />
        <circle cx="365" cy="0" r="2" fill="#000" stroke="#fff" />
        <circle cx="385" cy="0" r="2" fill="#000" stroke="#fff" />
        <circle cx="405" cy="0" r="2" fill="#000" stroke="#fff" />
      </g>

      <g style={{ transform: "rotate(67deg)", transformBox: "fill-box" }}>
        <line x1="428" y1="83" x2="608" y2="83" stroke="#000" />
        <circle cx="458" cy="83" r="2" fill="#000" stroke="#fff" />
        <circle cx="478" cy="83" r="2" fill="#000" stroke="#fff" />
        <circle cx="498" cy="83" r="2" fill="#000" stroke="#fff" />
        <circle cx="518" cy="83" r="3" fill="#000" stroke="#fff" />
        <circle cx="538" cy="83" r="2" fill="#000" stroke="#fff" />
        <circle cx="558" cy="83" r="2" fill="#000" stroke="#fff" />
        <circle cx="578" cy="83" r="2" fill="#000" stroke="#fff" />
      </g>

      <g style={{ transform: "rotate(113deg)", transformBox: "fill-box" }}>
        <line x1="503" y1="253" x2="693" y2="253" stroke="#000" />
        <circle cx="535" cy="253" r="2" fill="#000" stroke="#fff" />
        <circle cx="555" cy="253" r="2" fill="#000" stroke="#fff" />
        <circle cx="575" cy="253" r="2" fill="#000" stroke="#fff" />
        <circle cx="595" cy="253" r="3" fill="#000" stroke="#fff" />
        <circle cx="615" cy="253" r="2" fill="#000" stroke="#fff" />
        <circle cx="635" cy="253" r="2" fill="#000" stroke="#fff" />
        <circle cx="655" cy="253" r="2" fill="#000" stroke="#fff" />
      </g>

      <g style={{ transform: "rotate(157deg)", transformBox: "fill-box" }}>
        <line x1="425" y1="431" x2="615" y2="431" stroke="#000" />
        <circle cx="425" cy="431" r="2" fill="#000" stroke="#fff" />
        <circle cx="455" cy="431" r="2" fill="#000" stroke="#fff" />
        <circle cx="475" cy="431" r="2" fill="#000" stroke="#fff" />
        <circle cx="495" cy="431" r="2" fill="#000" stroke="#fff" />
        <circle cx="515" cy="431" r="3" fill="#000" stroke="#fff" />
        <circle cx="535" cy="431" r="2" fill="#000" stroke="#fff" />
        <circle cx="555" cy="431" r="2" fill="#000" stroke="#fff" />
        <circle cx="575" cy="431" r="2" fill="#000" stroke="#fff" />
        <circle cx="595" cy="431" r="2" fill="#000" stroke="#fff" />
      </g>

      <g style={{ transform: "rotate(203deg)", transformBox: "fill-box" }}>
        <line x1="250" y1="505" x2="443" y2="505" stroke="#000" />
        <circle cx="285" cy="505" r="2" fill="#000" stroke="#fff" />
        <circle cx="305" cy="505" r="2" fill="#000" stroke="#fff" />
        <circle cx="325" cy="505" r="2" fill="#000" stroke="#fff" />
        <circle cx="345" cy="505" r="3" fill="#000" stroke="#fff" />
        <circle cx="365" cy="505" r="2" fill="#000" stroke="#fff" />
        <circle cx="385" cy="505" r="2" fill="#000" stroke="#fff" />
        <circle cx="405" cy="505" r="2" fill="#000" stroke="#fff" />
      </g>

      <g>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(15px, 224px)", transformBox: "fill-box" }}>
          1,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(23px, 206px)", transformBox: "fill-box" }}>
          2,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(31px, 188px)", transformBox: "fill-box" }}>
          3,75
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(40px, 170px)", transformBox: "fill-box" }}>
          5 лет
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(47px, 150px)", transformBox: "fill-box" }}>
          6,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(55px, 133px)", transformBox: "fill-box" }}>
          7,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(61px, 117px)", transformBox: "fill-box" }}>
          8,75
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(-4px, 224px)", transformBox: "fill-box" }}>
          {values.get("period_1_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(5px, 206px)", transformBox: "fill-box" }}>
          {values.get("period_2_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(12px, 188px)", transformBox: "fill-box" }}>
          {values.get("period_3_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(19px, 170px)", transformBox: "fill-box" }}>
          {values.get("period_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(28px, 150px)", transformBox: "fill-box" }}>
          {values.get("period_6_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(35px, 133px)", transformBox: "fill-box" }}>
          {values.get("period_7_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(42px, 117px)", transformBox: "fill-box" }}>
          {values.get("period_8_75")}
        </text>
      </g>
      <g>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(103px, 70px)", transformBox: "fill-box" }}>
          11,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(120px, 63px)", transformBox: "fill-box" }}>
          12,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(138px, 55px)", transformBox: "fill-box" }}>
          13,75
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(156px, 48px)", transformBox: "fill-box" }}>
          15 лет
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(175px, 40px)", transformBox: "fill-box" }}>
          16,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(195px, 32px)", transformBox: "fill-box" }}>
          17,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(209px, 25px)", transformBox: "fill-box" }}>
          18,75
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(103px, 56px)", transformBox: "fill-box" }}>
          {values.get("period_11_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(120px, 49px)", transformBox: "fill-box" }}>
          {values.get("period_12_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(138px, 41px)", transformBox: "fill-box" }}>
          {values.get("period_13_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(157px, 32px)", transformBox: "fill-box" }}>
          {values.get("period_15")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(175px, 26px)", transformBox: "fill-box" }}>
          {values.get("period_16_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(194px, 19px)", transformBox: "fill-box" }}>
          {values.get("period_17_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(212px, 10px)", transformBox: "fill-box" }}>
          {values.get("period_18_75")}
        </text>
      </g>
      <g>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(275px, 25px)", transformBox: "fill-box" }}>
          21,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(293px, 32px)", transformBox: "fill-box" }}>
          22,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(310px, 40px)", transformBox: "fill-box" }}>
          23,75
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(327px, 48px)", transformBox: "fill-box" }}>
          25 лет
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(345px, 55px)", transformBox: "fill-box" }}>
          26,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(366px, 63px)", transformBox: "fill-box" }}>
          27,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(378px, 70px)", transformBox: "fill-box" }}>
          28,75
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(277px, 10px)", transformBox: "fill-box" }}>
          {values.get("period_21_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(295px, 19px)", transformBox: "fill-box" }}>
          {values.get("period_22_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(314px, 26px)", transformBox: "fill-box" }}>
          {values.get("period_23_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(333px, 32px)", transformBox: "fill-box" }}>
          {values.get("period_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(350px, 41px)", transformBox: "fill-box" }}>
          {values.get("period_26_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(369px, 49px)", transformBox: "fill-box" }}>
          {values.get("period_27_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(388px, 56px)", transformBox: "fill-box" }}>
          {values.get("period_28_75")}
        </text>
      </g>
      <g>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(421px, 117px)", transformBox: "fill-box" }}>
          31,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(429px, 133px)", transformBox: "fill-box" }}>
          32,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(433px, 150px)", transformBox: "fill-box" }}>
          33,75
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(437px, 170px)", transformBox: "fill-box" }}>
          35 лет
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(449px, 188px)", transformBox: "fill-box" }}>
          36,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(461px, 206px)", transformBox: "fill-box" }}>
          37,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(465px, 224px)", transformBox: "fill-box" }}>
          38,75
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(444px, 117px)", transformBox: "fill-box" }}>
          {values.get("period_31_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(449px, 133px)", transformBox: "fill-box" }}>
          {values.get("period_32_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(457px, 150px)", transformBox: "fill-box" }}>
          {values.get("period_33_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(467px, 170px)", transformBox: "fill-box" }}>
          {values.get("period_35")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(473px, 188px)", transformBox: "fill-box" }}>
          {values.get("period_36_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(482px, 206px)", transformBox: "fill-box" }}>
          {values.get("period_37_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(490px, 224px)", transformBox: "fill-box" }}>
          {values.get("period_38_75")}
        </text>
      </g>
      <g>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(467px, 281px)", transformBox: "fill-box" }}>
          41,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(463px, 298px)", transformBox: "fill-box" }}>
          42,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(452px, 317px)", transformBox: "fill-box" }}>
          43,75
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(440px, 334px)", transformBox: "fill-box" }}>
          45 лет
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(436px, 354px)", transformBox: "fill-box" }}>
          46,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(432px, 373px)", transformBox: "fill-box" }}>
          47,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(423px, 387px)", transformBox: "fill-box" }}>
          48,75
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(493px, 281px)", transformBox: "fill-box" }}>
          {values.get("period_41_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(486px, 298px)", transformBox: "fill-box" }}>
          {values.get("period_42_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(479px, 317px)", transformBox: "fill-box" }}>
          {values.get("period_43_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(472px, 334px)", transformBox: "fill-box" }}>
          {values.get("period_45")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(461px, 354px)", transformBox: "fill-box" }}>
          {values.get("period_46_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(454px, 373px)", transformBox: "fill-box" }}>
          {values.get("period_47_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(448px, 387px)", transformBox: "fill-box" }}>
          {values.get("period_48_75")}
        </text>
      </g>
      <g>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(378px, 435px)", transformBox: "fill-box" }}>
          51,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(365px, 442px)", transformBox: "fill-box" }}>
          52,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(345px, 449px)", transformBox: "fill-box" }}>
          53,75
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(327px, 456px)", transformBox: "fill-box" }}>
          55 лет
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(310px, 465px)", transformBox: "fill-box" }}>
          56,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(293px, 473px)", transformBox: "fill-box" }}>
          57,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(275px, 479px)", transformBox: "fill-box" }}>
          58,75
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(388px, 451px)", transformBox: "fill-box" }}>
          {values.get("period_51_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(370px, 458px)", transformBox: "fill-box" }}>
          {values.get("period_52_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(352px, 465px)", transformBox: "fill-box" }}>
          {values.get("period_53_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(333px, 475px)", transformBox: "fill-box" }}>
          {values.get("period_55")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(315px, 482px)", transformBox: "fill-box" }}>
          {values.get("period_56_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(293px, 490px)", transformBox: "fill-box" }}>
          {values.get("period_57_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(275px, 497px)", transformBox: "fill-box" }}>
          {values.get("period_58_75")}
        </text>
      </g>
      <g>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(209px, 479px)", transformBox: "fill-box" }}>
          61,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(196px, 473px)", transformBox: "fill-box" }}>
          62,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(176px, 465px)", transformBox: "fill-box" }}>
          63,75
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(157px, 456px)", transformBox: "fill-box" }}>
          65 лет
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(138px, 449px)", transformBox: "fill-box" }}>
          66,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(121px, 442px)", transformBox: "fill-box" }}>
          67,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(104px, 435px)", transformBox: "fill-box" }}>
          68,75
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(212px, 497px)", transformBox: "fill-box" }}>
          {values.get("period_61_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(194px, 490px)", transformBox: "fill-box" }}>
          {values.get("period_62_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(175px, 482px)", transformBox: "fill-box" }}>
          {values.get("period_63_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(157px, 475px)", transformBox: "fill-box" }}>
          {values.get("period_65")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(138px, 465px)", transformBox: "fill-box" }}>
          {values.get("period_66_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(120px, 458px)", transformBox: "fill-box" }}>
          {values.get("period_67_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(103px, 451px)", transformBox: "fill-box" }}>
          {values.get("period_68_75")}
        </text>
      </g>
      <g>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(61px, 387px)", transformBox: "fill-box" }}>
          71,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(55px, 373px)", transformBox: "fill-box" }}>
          72,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(47px, 354px)", transformBox: "fill-box" }}>
          73,75
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(40px, 334px)", transformBox: "fill-box" }}>
          75 лет
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(31px, 317px)", transformBox: "fill-box" }}>
          76,25
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(23px, 298px)", transformBox: "fill-box" }}>
          77,5
        </text>
        <text x="0" y="0" className={styles.PeriodText} style={{ transform: "translate(15px, 281px)", transformBox: "fill-box" }}>
          78,75
        </text>

        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(45px, 394px)", transformBox: "fill-box" }}>
          {values.get("period_71_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(36px, 373px)", transformBox: "fill-box" }}>
          {values.get("period_72_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(29px, 354px)", transformBox: "fill-box" }}>
          {values.get("period_73_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(22px, 334px)", transformBox: "fill-box" }}>
          {values.get("period_75")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(12px, 317px)", transformBox: "fill-box" }}>
          {values.get("period_76_25")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(5px, 298px)", transformBox: "fill-box" }}>
          {values.get("period_77_5")}
        </text>
        <text x="0" y="0" className={styles.PeriodValue} style={{ transform: "translate(-4px, 281px)", transformBox: "fill-box" }}>
          {values.get("period_78_75")}
        </text>
      </g>

      <line x1="75" y1="425" x2="0" y2="250" stroke="#acacac" />
      <g style={{ transform: "rotate(67deg)", transformBox: "fill-box", transformOrigin: "left center" }}>
        <line x1="0" y1="250" x2="190" y2="250" stroke="#000" />
        <circle cx="35" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="55" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="75" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="95" cy="250" r="3" fill="#000" stroke="#fff" />
        <circle cx="115" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="135" cy="250" r="2" fill="#000" stroke="#fff" />
        <circle cx="155" cy="250" r="2" fill="#000" stroke="#fff" />
      </g>

      <g className={styles.React2}>
        <circle cx="100" cy="100" r="24" fill="#939" stroke="#fff" style={{ transform: "translate(-10px, -10px)" }} />
        <circle cx="100" cy="100" r="18" fill="#36c" stroke="#fff" style={{ transform: "translate(20px, 20px)" }} />
        <circle cx="100" cy="100" r="14" fill="#3eb4f0" stroke="#fff" style={{ transform: "translate(44px, 44px)" }} />
        <circle cx="100" cy="100" r="14" fill="#73b55f" stroke="#fff" style={{ transform: "translate(90px, 90px)" }} />

        <circle cx="400" cy="100" r="24" fill="#d14a44" stroke="#fff" style={{ transform: "translate(10px, -10px)" }} />
        <circle cx="400" cy="100" r="18" fill="white" stroke="black" style={{ transform: "translate(-20px, 20px)" }} />
        <circle cx="400" cy="100" r="14" fill="#d88a4b" stroke="#fff" style={{ transform: "translate(-44px, 44px)" }} />

        <circle cx="400" cy="400" r="24" fill="#d14a44" stroke="#fff" style={{ transform: "translate(10px, 10px)" }} />
        <circle cx="400" cy="400" r="18" fill="white" stroke="black" style={{ transform: "translate(-20px, -20px)" }} />
        <circle cx="400" cy="400" r="14" fill="#d88a4b" stroke="#fff" style={{ transform: "translate(-44px, -44px)" }} />

        <circle cx="100" cy="400" r="24" fill="#939" stroke="#fff" style={{ transform: "translate(-10px, 10px)" }} />
        <circle cx="100" cy="400" r="18" fill="#36c" stroke="#fff" style={{ transform: "translate(20px, -20px)" }} />
        <circle cx="100" cy="400" r="14" fill="#3eb4f0" stroke="#fff" style={{ transform: "translate(44px, -44px)" }} />
        <circle cx="100" cy="400" r="14" fill="#73b55f" stroke="#fff" style={{ transform: "translate(90px, -90px)" }} />
      </g>
      <circle cx="100" cy="100" r="24" fill="white" stroke="black" style={{ transform: "translate(-10px, -10px)" }} />
      <circle cx="100" cy="100" r="18" fill="white" stroke="black" style={{ transform: "translate(20px, 20px)" }} />
      <circle cx="100" cy="100" r="14" fill="white" stroke="black" style={{ transform: "translate(44px, 44px)" }} />

      <circle cx="400" cy="100" r="24" fill="white" stroke="black" style={{ transform: "translate(10px, -10px)" }} />
      <circle cx="400" cy="100" r="18" fill="white" stroke="black" style={{ transform: "translate(-20px, 20px)" }} />
      <circle cx="400" cy="100" r="14" fill="white" stroke="black" style={{ transform: "translate(-44px, 44px)" }} />

      <circle cx="400" cy="400" r="24" fill="white" stroke="black" style={{ transform: "translate(10px, 10px)" }} />
      <circle cx="400" cy="400" r="18" fill="white" stroke="black" style={{ transform: "translate(-20px, -20px)" }} />
      <circle cx="400" cy="400" r="14" fill="white" stroke="black" style={{ transform: "translate(-44px, -44px)" }} />

      <circle cx="100" cy="400" r="24" fill="white" stroke="black" style={{ transform: "translate(-10px, 10px)" }} />
      <circle cx="100" cy="400" r="18" fill="white" stroke="black" style={{ transform: "translate(20px, -20px)" }} />
      <circle cx="100" cy="400" r="14" fill="white" stroke="black" style={{ transform: "translate(44px, -44px)" }} />

      <circle cx="250" cy="400" r="14" fill="#fff" stroke="black" style={{ transform: "translate(40px, -40px)" }} />
      <circle cx="250" cy="400" r="14" fill="#fff" stroke="black" style={{ transform: "translate(75px, -75px)" }} />
      <circle cx="250" cy="400" r="14" fill="#fff" stroke="black" style={{ transform: "translate(110px, -110px)" }} />

      <circle cx="100" cy="100" r="24" fill="#f7de74" stroke="#fff" style={{ transform: "translate(150px, 150px)" }} />

      <g id="points-basic-mode">
        <text className={styles.DiagramValue} style={{ transform: "translate(15px, 256px)" }} fill="#ffffff">
          {values.get("point_01")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 30px)" }} fill="#ffffff">
          {values.get("point_02")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(468px, 256px)" }} fill="#ffffff">
          {values.get("point_03")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(242px, 481px)" }} fill="#ffffff">
          {values.get("point_04")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 256px)" }}>
          {values.get("point_05")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(81px, 95px)" }}>
          {values.get("point_18")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(401px, 95px)" }}>
          {values.get("point_19")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(401px, 416px)" }}>
          {values.get("point_20")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(81px, 416px)" }}>
          {values.get("point_21")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(242px, 406px)" }} fill="#ffffff">
          {values.get("point_09")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(391px, 256px)" }} fill="#ffffff">
          {values.get("point_08")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(316px, 331px)" }}>
          {values.get("point_10")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(281px, 366px)" }}>
          {values.get("point_12")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(351px, 296px)" }}>
          {values.get("point_11")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(91px, 256px)" }} fill="#ffffff">
          {values.get("point_07")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 106px)" }} fill="#ffffff">
          {values.get("point_06")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(57px, 256px)" }} fill="#ffffff">
          {values.get("point_15")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 72px)" }} fill="#ffffff">
          {values.get("point_14")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(242px, 439px)" }}>
          {values.get("point_13")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(156px, 256px)" }} fill="#ffffff">
          {values.get("point_17")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(241px, 171px)" }} fill="#ffffff">
          {values.get("point_16")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(112px, 126px)" }}>
          {values.get("point_c1_10_2")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(371px, 126px)" }}>
          {values.get("point_c1_30_2")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(371px, 386px)" }}>
          {values.get("point_c1_50_2")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(112px, 386px)" }}>
          {values.get("point_c1_70_2")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(136px, 150px)" }}>
          {values.get("point_c1_10_3")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(347px, 150px)" }}>
          {values.get("point_c1_30_3")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(347px, 362px)" }}>
          {values.get("point_c1_50_3")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(136px, 362px)" }}>
          {values.get("point_c1_70_3")}
        </text>
        <text className={styles.DiagramValue} style={{ transform: "translate(424px, 256px)" }}>
          {values.get("point_c1_40_2")}
        </text>
      </g>
    </svg>
  );
}
