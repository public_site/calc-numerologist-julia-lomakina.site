/**
 * Расчет матрицы души
 * @param {*} strDate
 * @returns
 */
const CalcMatrixForString = (strDate) => {
  var aDate = strDate.split(".");
  if (aDate.length != 3) {
    console.log("aDate.length != 3");
  }
  var nDay = parseInt(aDate[0]);
  var nMonth = parseInt(aDate[1]);
  var nYear = parseInt(aDate[2]);
  return calcMatrix(nDay, nMonth, nYear);
};

/**
 * Расчет матрицы совместимости
 */
const calcMatrixCompatibility = (pointsMatrix1, pointsMatrix2) => {
  var points = new Map();

  // Вычисляемые точки на основе двух матриц
  var point_01 = calcValueForMatrixTwo(pointsMatrix1.get(`point_01`), pointsMatrix2.get(`point_01`));
  var point_02 = calcValueForMatrixTwo(pointsMatrix1.get(`point_02`), pointsMatrix2.get(`point_02`));
  var point_03 = calcValueForMatrixTwo(pointsMatrix1.get(`point_03`), pointsMatrix2.get(`point_03`));
  var point_04 = calcValueForMatrixTwo(pointsMatrix1.get(`point_04`), pointsMatrix2.get(`point_04`));

  var point_05 = fixedNumberForRange(point_01 + point_02 + point_03 + point_04);
  var point_06 = fixedNumberForRange(point_02 + point_05);
  var point_07 = fixedNumberForRange(point_01 + point_05);
  var point_08 = fixedNumberForRange(point_05 + point_03);
  var point_09 = fixedNumberForRange(point_05 + point_04);
  var point_10 = fixedNumberForRange(point_08 + point_09);
  var point_11 = fixedNumberForRange(point_08 + point_10);
  var point_12 = fixedNumberForRange(point_09 + point_10);
  var point_13 = fixedNumberForRange(point_04 + point_09);
  var point_14 = fixedNumberForRange(point_02 + point_06);
  var point_15 = fixedNumberForRange(point_01 + point_07);
  var point_16 = fixedNumberForRange(point_05 + point_06);
  var point_17 = fixedNumberForRange(point_05 + point_07);
  var point_18 = fixedNumberForRange(point_01 + point_02);
  var point_19 = fixedNumberForRange(point_02 + point_03);
  var point_20 = fixedNumberForRange(point_03 + point_04);
  var point_21 = fixedNumberForRange(point_01 + point_04);

  points.set(`point_01`, point_01);
  points.set(`point_02`, point_02);
  points.set(`point_03`, point_03);
  points.set(`point_04`, point_04);
  points.set(`point_05`, point_05);
  points.set(`point_06`, point_06);
  points.set(`point_07`, point_07);
  points.set(`point_08`, point_08);
  points.set(`point_09`, point_09);
  points.set(`point_10`, point_10);
  points.set(`point_11`, point_11);
  points.set(`point_12`, point_12);
  points.set(`point_13`, point_13);
  points.set(`point_14`, point_14);
  points.set(`point_15`, point_15);
  points.set(`point_16`, point_16);
  points.set(`point_17`, point_17);
  points.set(`point_18`, point_18);
  points.set(`point_19`, point_19);
  points.set(`point_20`, point_20);
  points.set(`point_21`, point_21);

  let point_c1_10_3 = fixedNumberForRange(fixedNumberForRange(point_18 + point_19 + point_20 + point_21) + point_18);
  let point_c1_30_3 = fixedNumberForRange(fixedNumberForRange(point_18 + point_19 + point_20 + point_21) + point_19);
  let point_c1_50_3 = fixedNumberForRange(fixedNumberForRange(point_18 + point_19 + point_20 + point_21) + point_20);
  let point_c1_70_3 = fixedNumberForRange(fixedNumberForRange(point_18 + point_19 + point_20 + point_21) + point_21);
  let point_c1_10_2 = fixedNumberForRange(point_18 + point_c1_10_3);
  let point_c1_30_2 = fixedNumberForRange(point_19 + point_c1_30_3);
  let point_c1_50_2 = fixedNumberForRange(point_20 + point_c1_50_3);
  let point_c1_70_2 = fixedNumberForRange(point_21 + point_c1_70_3);
  let point_c1_40_2 = fixedNumberForRange(point_03 + point_08);

  points.set("point_c1_10_3", point_c1_10_3);
  points.set("point_c1_30_3", point_c1_30_3);
  points.set("point_c1_50_3", point_c1_50_3);
  points.set("point_c1_70_3", point_c1_70_3);
  points.set("point_c1_10_2", point_c1_10_2);
  points.set("point_c1_30_2", point_c1_30_2);
  points.set("point_c1_50_2", point_c1_50_2);
  points.set("point_c1_70_2", point_c1_70_2);
  points.set("point_c1_40_2", point_c1_40_2);

  return points;
};

/* PRIVATE */
/**
 * Расчет матрицы души
 * @param {ц} nDay
 * @param {*} nMonth
 * @param {*} nYear
 * @returns
 */
function calcMatrix(nDay, nMonth, nYear) {
  let points = new Map();

  // Первая точка (фиолетовый круг, в примере цифра 21)
  // Это дата рождения с 1 до 22, если больше 22 – то есть: 23 – это 2+3 = 5 – вписывается цифра 5 и т.д.
  var point_01 = fixedNumberForRange(nDay, 22);
  points.set("point_01", point_01);

  // Вторая точка (фиолетовый круг, в примере цифра 10)
  // Это месяц рождения с 1 до 12
  var point_02 = nMonth;
  points.set("point_02", point_02);

  // Третья точка (красный круг, в примере цифра 7)
  // Это год рождения, вычисляется путем сложения:
  // В нашем примере 1987 – то есть 1+ 9 + 8 + 7 = 25 и полученную цифру складываем пока не получится цифра в диапазоне от 1 до 22 включительно!
  var point_03 = 0;
  var sYear = nYear.toString();
  for (let i = 0; i < sYear.length; i++) {
    point_03 += parseInt(sYear[i]);
  }
  point_03 = fixedNumberForRange(point_03, 22);
  points.set("point_03", point_03);

  // Четвертая точка (красный круг, в примере цифра 11)
  // Это сложение первых трех точек
  // В нашем примере это: 21+10+7 = 38, получилась цифра большее 22 - поэтому далее сводим путем сложения пока не получится цифра в диапазоне от 1 до 22: 3+8 = 11
  var point_04 = parseInt(point_01) + parseInt(point_02) + parseInt(point_03);
  point_04 = fixedNumberForRange(point_04, 22);
  points.set("point_04", point_04);

  // Пятая точка (желтый круг по центру, в примере цифра 13)
  // #point_05
  // Это сложение первых четырех точек
  // В нашем примере это: 21+10+7+11 = 49, получилась цифра большее 22 - поэтому далее сводим путем сложения пока не получится цифра в диапазоне от 1 до 22: 4+9 = 13
  var point_05 = parseInt(point_01) + parseInt(point_02) + parseInt(point_03) + parseInt(point_04);
  point_05 = fixedNumberForRange(point_05, 22);
  points.set("point_05", point_05);

  // Шестая точка (голубой круг, в примере цифра 5)
  // #point_06
  // Это сложение 2ой и 5ой точки
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_06 = parseInt(point_02) + parseInt(point_05);
  point_06 = fixedNumberForRange(point_06, 22);
  points.set("point_06", point_06);

  // Седьмая точка (голубой круг, в примере цифра 7)
  // #point_07
  // Это сложение 1ой и 5ой точки
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_07 = parseInt(point_01) + parseInt(point_05);
  point_07 = fixedNumberForRange(point_07, 22);
  points.set("point_07", point_07);

  // Восьмая точка (оранжевый круг, в примере цифра 20)
  // #point_08
  // Складываем пятую (#point_05) точку и третью точку (#point_03)
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_08 = parseInt(point_05) + parseInt(point_03);
  point_08 = fixedNumberForRange(point_08, 22);
  points.set("point_08", point_08);

  // Девятая точка – (оранжевый круг, в примере цифра 6)
  // #point_09
  // Складываем пятую (#point_05) точку и четвертую точку (#point_04)
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_09 = parseInt(point_05) + parseInt(point_04);
  point_09 = fixedNumberForRange(point_09, 22);
  points.set("point_09", point_09);

  // Десятая точка (бесцветный круг, в примере цифра 8)
  // #point_10
  // Складываем точку #point_08 и точку #point_09
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_10 = parseInt(point_08) + parseInt(point_09);
  point_10 = fixedNumberForRange(point_10, 22);
  points.set("point_10", point_10);

  // Одиннадцатая точка (бесцветный круг, в примере цифра 10)
  // #point_11
  // Складываем точку #point_08 и точку #point_10
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_11 = parseInt(point_08) + parseInt(point_10);
  point_11 = fixedNumberForRange(point_11, 22);
  points.set("point_11", point_11);

  // Двенадцатая точка – (оранжевый круг, в примере цифра 14)
  // #point_12
  // Складываем точку #point_09 и точку #point_10
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_12 = parseInt(point_09) + parseInt(point_10);
  point_12 = fixedNumberForRange(point_12, 22);
  points.set("point_12", point_12);

  // Тринадцатая точка (бесцветный круг, в примере цифра 17)
  // #point_13
  // Складываем четвертую точку и девятую точку
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_13 = parseInt(point_04) + parseInt(point_09);
  point_13 = fixedNumberForRange(point_13, 22);
  points.set("point_13", point_13);

  // Четырнадцатая точка (синий круг, в примере цифра 15)
  // #point_14
  // Складываем вторую точку и шестую
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_14 = parseInt(point_02) + parseInt(point_06);
  point_14 = fixedNumberForRange(point_14, 22);
  points.set("point_14", point_14);

  // Пятнадцатая точка – (синий круг, в примере цифра 10)
  // #point_15
  // Складываем первую и седьмую точку
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_15 = parseInt(point_01) + parseInt(point_07);
  point_15 = fixedNumberForRange(point_15, 22);
  points.set("point_15", point_15);

  // Шестнадцатая точка (зеленый круг, в примере цифра 18)
  // #point_16
  // Складываем пятую центральную точку и шестую
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_16 = parseInt(point_05) + parseInt(point_06);
  point_16 = fixedNumberForRange(point_16, 22);
  points.set("point_16", point_16);

  // Семнадцатая точка (зеленый круг, в примере цифра 20)
  // #point_17
  // Складываем пятую центральную точку и седьмую
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_17 = parseInt(point_05) + parseInt(point_07);
  point_17 = fixedNumberForRange(point_17, 22);
  points.set("point_17", point_17);

  // Восемнадцатая точка – (бесцветный круг, в примере цифра 4)
  // #point_18
  // Складываем первую и вторую точку
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_18 = parseInt(point_01) + parseInt(point_02);
  point_18 = fixedNumberForRange(point_18, 22);
  points.set("point_18", point_18);

  // Девятнадцатая точка (бесцветный круг, в примере цифра 17)
  // #point_19
  // Складываем вторую и третью точку
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_19 = parseInt(point_02) + parseInt(point_03);
  point_19 = fixedNumberForRange(point_19, 22);
  points.set("point_19", point_19);

  // Двадцатая точка (бесцветный круг, в примере цифра 18)
  // #point_20
  // Складываем третью и четвертую точку
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_20 = parseInt(point_03) + parseInt(point_04);
  point_20 = fixedNumberForRange(point_20, 22);
  points.set("point_20", point_20);

  // Двадцать первая точка – (бесцветный круг, в примере цифра 5)
  // #point_21
  // Складываем первую и четвертую точку
  // Если полученная цифра больше 22, то её сводим путем сложения пока не получится цифра в диапазоне от 1 до 22
  var point_21 = parseInt(point_01) + parseInt(point_04);
  point_21 = fixedNumberForRange(point_21, 22);
  points.set("point_21", point_21);

  // Предназначение для себя - Небо
  // #SpanDestinyMySky
  // Складываем вторую и четвертую точку
  var destinyMySky = parseInt(point_02) + parseInt(point_04);
  destinyMySky = fixedNumberForRange(destinyMySky);
  points.set("SpanDestinyMySky", destinyMySky);

  // Предназначение для себя - Земля
  // #SpanDestinyMyEarth
  // Складываем первую и третью точку
  var destinyMyEarth = parseInt(point_01) + parseInt(point_03);
  destinyMyEarth = fixedNumberForRange(destinyMyEarth);
  points.set("SpanDestinyMyEarth", destinyMyEarth);

  // Предназначение для себя
  // #SpanDestinyMySum
  // Складываем предназначение для себя по небу и предназначение для себя по земле
  var destinyMySum = parseInt(destinyMySky) + parseInt(destinyMyEarth);
  destinyMySum = fixedNumberForRange(destinyMySum, 22);
  points.set("SpanDestinyMySum", destinyMySum);

  // Предназначение для социума - М
  // #SpanDestinySocietyM
  // Складываем 20 и 18 точки
  var destinySocietyM = parseInt(point_20) + parseInt(point_18);
  destinySocietyM = fixedNumberForRange(destinySocietyM);
  points.set("SpanDestinySocietyM", destinySocietyM);

  // Предназначение для социума - Ж
  // #SpanDestinySocietyZh
  // Складываем 21 и 19 точки
  var destinySocietyZh = parseInt(point_21) + parseInt(point_19);
  destinySocietyZh = fixedNumberForRange(destinySocietyZh);
  points.set("SpanDestinySocietyZh", destinySocietyZh);

  // Предназначение для социума
  // #SpanDestinySocietySum
  // складываем предназначение для социума М и предназначение для социума Ж
  var destinySocietySum = parseInt(destinySocietyM) + parseInt(destinySocietyZh);
  destinySocietySum = fixedNumberForRange(destinySocietySum, 22);
  points.set("SpanDestinySocietySum", destinySocietySum);

  // Общее предназначение (в примере цифра 12)
  // Складываем предназначение для себя третье и предназначение для социума третье

  var destinyGeneral = parseInt(destinyMySum) + parseInt(destinySocietySum);
  destinyGeneral = fixedNumberForRange(destinyGeneral, 22);
  points.set("SpanDestinyGeneral", destinyGeneral);

  let point_c1_10_3 = fixedNumberForRange(fixedNumberForRange(point_18 + point_19 + point_20 + point_21) + point_18);
  let point_c1_30_3 = fixedNumberForRange(fixedNumberForRange(point_18 + point_19 + point_20 + point_21) + point_19);
  let point_c1_50_3 = fixedNumberForRange(fixedNumberForRange(point_18 + point_19 + point_20 + point_21) + point_20);
  let point_c1_70_3 = fixedNumberForRange(fixedNumberForRange(point_18 + point_19 + point_20 + point_21) + point_21);
  let point_c1_10_2 = fixedNumberForRange(point_18 + point_c1_10_3);
  let point_c1_30_2 = fixedNumberForRange(point_19 + point_c1_30_3);
  let point_c1_50_2 = fixedNumberForRange(point_20 + point_c1_50_3);
  let point_c1_70_2 = fixedNumberForRange(point_21 + point_c1_70_3);
  let point_c1_40_2 = fixedNumberForRange(point_03 + point_08);

  points.set("point_c1_10_3", point_c1_10_3);
  points.set("point_c1_30_3", point_c1_30_3);
  points.set("point_c1_50_3", point_c1_50_3);
  points.set("point_c1_70_3", point_c1_70_3);
  points.set("point_c1_10_2", point_c1_10_2);
  points.set("point_c1_30_2", point_c1_30_2);
  points.set("point_c1_50_2", point_c1_50_2);
  points.set("point_c1_70_2", point_c1_70_2);
  points.set("point_c1_40_2", point_c1_40_2);

  //
  //
  //
  let period_0 = point_01;
  let period_10 = point_18;
  let period_20 = point_02;
  let period_30 = point_19;
  let period_40 = point_03;
  let period_50 = point_20;
  let period_60 = point_04;
  let period_70 = point_21;
  let period_80 = point_01;

  let period_5 = calcValue(period_0, period_10);
  let period_15 = calcValue(period_10, period_20);
  let period_25 = calcValue(period_20, period_30);
  let period_35 = calcValue(period_30, period_40);
  let period_45 = calcValue(period_40, period_50);
  let period_55 = calcValue(period_50, period_60);
  let period_65 = calcValue(period_60, period_70);
  let period_75 = calcValue(period_70, period_80);

  let period_2_5 = calcValue(period_0, period_5);
  let period_7_5 = calcValue(period_5, period_10);
  let period_12_5 = calcValue(period_10, period_15);
  let period_17_5 = calcValue(period_15, period_20);
  let period_22_5 = calcValue(period_20, period_25);
  let period_27_5 = calcValue(period_25, period_30);
  let period_32_5 = calcValue(period_30, period_35);
  let period_37_5 = calcValue(period_35, period_40);
  let period_42_5 = calcValue(period_40, period_45);
  let period_47_5 = calcValue(period_45, period_50);
  let period_52_5 = calcValue(period_50, period_55);
  let period_57_5 = calcValue(period_55, period_60);
  let period_62_5 = calcValue(period_60, period_65);
  let period_67_5 = calcValue(period_65, period_70);
  let period_72_5 = calcValue(period_70, period_75);
  let period_77_5 = calcValue(period_75, period_0);

  let period_1_25 = calcValue(period_0, period_2_5);
  let period_3_75 = calcValue(period_2_5, period_5);
  let period_6_25 = calcValue(period_5, period_7_5);
  let period_8_75 = calcValue(period_7_5, period_10);
  let period_11_25 = calcValue(period_10, period_12_5);
  let period_13_75 = calcValue(period_12_5, period_15);
  let period_16_25 = calcValue(period_15, period_17_5);
  let period_18_75 = calcValue(period_17_5, period_20);
  let period_21_25 = calcValue(period_20, period_22_5);
  let period_23_75 = calcValue(period_22_5, period_25);
  let period_26_25 = calcValue(period_25, period_27_5);
  let period_28_75 = calcValue(period_27_5, period_30);
  let period_31_25 = calcValue(period_30, period_32_5);
  let period_33_75 = calcValue(period_32_5, period_35);
  let period_36_25 = calcValue(period_35, period_37_5);
  let period_38_75 = calcValue(period_37_5, period_40);
  let period_41_25 = calcValue(period_40, period_42_5);
  let period_43_75 = calcValue(period_42_5, period_45);
  let period_46_25 = calcValue(period_45, period_47_5);
  let period_48_75 = calcValue(period_47_5, period_50);
  let period_51_25 = calcValue(period_50, period_52_5);
  let period_53_75 = calcValue(period_52_5, period_55);
  let period_56_25 = calcValue(period_55, period_57_5);
  let period_58_75 = calcValue(period_57_5, period_60);
  let period_61_25 = calcValue(period_60, period_62_5);
  let period_63_75 = calcValue(period_62_5, period_65);
  let period_66_25 = calcValue(period_65, period_67_5);
  let period_68_75 = calcValue(period_67_5, period_70);
  let period_71_25 = calcValue(period_70, period_72_5);
  let period_73_75 = calcValue(period_72_5, period_75);
  let period_76_25 = calcValue(period_75, period_77_5);
  let period_78_75 = calcValue(period_77_5, period_80);
  points.set("period_5", period_5);
  points.set("period_15", period_15);
  points.set("period_25", period_25);
  points.set("period_35", period_35);
  points.set("period_45", period_45);
  points.set("period_55", period_55);
  points.set("period_65", period_65);
  points.set("period_75", period_75);
  points.set("period_2_5", period_2_5);
  points.set("period_7_5", period_7_5);
  points.set("period_12_5", period_12_5);
  points.set("period_17_5", period_17_5);
  points.set("period_22_5", period_22_5);
  points.set("period_27_5", period_27_5);
  points.set("period_32_5", period_32_5);
  points.set("period_37_5", period_37_5);
  points.set("period_42_5", period_42_5);
  points.set("period_47_5", period_47_5);
  points.set("period_52_5", period_52_5);
  points.set("period_57_5", period_57_5);
  points.set("period_62_5", period_62_5);
  points.set("period_67_5", period_67_5);
  points.set("period_72_5", period_72_5);
  points.set("period_77_5", period_77_5);
  points.set("period_1_25", period_1_25);
  points.set("period_3_75", period_3_75);
  points.set("period_6_25", period_6_25);
  points.set("period_8_75", period_8_75);
  points.set("period_11_25", period_11_25);
  points.set("period_13_75", period_13_75);
  points.set("period_16_25", period_16_25);
  points.set("period_18_75", period_18_75);
  points.set("period_21_25", period_21_25);
  points.set("period_23_75", period_23_75);
  points.set("period_26_25", period_26_25);
  points.set("period_28_75", period_28_75);
  points.set("period_31_25", period_31_25);
  points.set("period_33_75", period_33_75);
  points.set("period_36_25", period_36_25);
  points.set("period_38_75", period_38_75);
  points.set("period_41_25", period_41_25);
  points.set("period_43_75", period_43_75);
  points.set("period_46_25", period_46_25);
  points.set("period_48_75", period_48_75);
  points.set("period_51_25", period_51_25);
  points.set("period_53_75", period_53_75);
  points.set("period_56_25", period_56_25);
  points.set("period_58_75", period_58_75);
  points.set("period_61_25", period_61_25);
  points.set("period_63_75", period_63_75);
  points.set("period_66_25", period_66_25);
  points.set("period_68_75", period_68_75);
  points.set("period_71_25", period_71_25);
  points.set("period_73_75", period_73_75);
  points.set("period_76_25", period_76_25);
  points.set("period_78_75", period_78_75);

  return points;
}

/**
 * Приведение цифры к ограничению
 * @param {*} num
 * @param {*} range
 * @returns
 */
function fixedNumberForRange(num, range = 22) {
  num = parseInt(num);
  range = parseInt(range);

  if (num > range) {
    var str = num.toString();
    return parseInt(str[0]) + parseInt(str[1]);
  } else {
    return num;
  }
}
function calcValue(valuePhysics, valueEnergy) {
  let physics = parseInt(valuePhysics.toString());
  let energy = parseInt(valueEnergy.toString());
  let healthKey = physics + energy;
  if (healthKey > 22) {
    let str = healthKey.toString();
    healthKey = parseInt(str[0]) + parseInt(str[1]);
  }
  return healthKey;
}

function calcValueForMatrixTwo(value1, value2) {
  let valueInt1 = parseInt(value1.toString());
  let valueInt2 = parseInt(value2.toString());
  let res = valueInt1 + valueInt2;
  if (res > 22) {
    res = fixedNumberForRange(res);
  }
  return res;
}

//
// EXPORT
//
export { CalcMatrixForString, calcMatrixCompatibility };
