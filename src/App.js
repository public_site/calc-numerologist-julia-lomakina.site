import logo from './logo.svg';
import './App.css';
import MainPage from './page/MainPage';
import { ThemeProvider } from "@mui/material/styles";
import { lightTheme } from "./themes";

function App() {
  return (
    <ThemeProvider theme={lightTheme}>
      <MainPage />
    </ThemeProvider>
  );
}

export default App;
