# ReactJS

## Packed:

- npm i --save react-input-mask react-transition-group bootstrap react-bootstrap sass

## Сборка

### npm i --save react-uuid

### npm run build

### Подготовка к загрузки на GitLab Page:

> Замена в build/\*.html
>
> > ="/  
> > ="

> Замена в build/static/js/\*.js
>
> > n.p+"static/media/  
> > "static/media/

> Замена в build/static/css/\*.css
>
> > url(/static/media/  
> > url(../media/
